import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void sum_shouldAddNumbers() {

        //given
        double firstOperand1 = 23;
        double firstOperand2 = -7;
        double firstOperand3 = 0;
        double secondOperand1 = 45;
        double secondOperand2 = 12;
        double secondOperand3 = -4;

        //when
        double expected1 = firstOperand1 + secondOperand1;
        double expected2 = firstOperand2 + secondOperand2;
        double expected3 = firstOperand3 - secondOperand3;

        //then
        assertEquals(expected1, Calculator.functionPlus(firstOperand1, secondOperand1));
        assertEquals(expected2, Calculator.functionPlus(firstOperand2, secondOperand2));
        assertNotEquals(expected3, Calculator.functionPlus(firstOperand3, secondOperand3));
    }

    @org.junit.jupiter.api.Test
    void difference_shouldSubtractNumbers() {

        //given
        double firstOperand1 = 39;
        double firstOperand2 = -56;
        double firstOperand3 = 0;
        double secondOperand1 = -45;
        double secondOperand2 = 2;
        double secondOperand3 = 4;

        //when
        double expected1 = firstOperand1 - secondOperand1;
        double expected2 = firstOperand2 - secondOperand2;
        double expected3 = firstOperand3 + secondOperand3;

        //then
        assertEquals(expected1, Calculator.functionMinus(firstOperand1, secondOperand1));
        assertEquals(expected2, Calculator.functionMinus(firstOperand2, secondOperand2));
        assertNotEquals(expected3, Calculator.functionMinus(firstOperand3, secondOperand3));
    }

    @org.junit.jupiter.api.Test
    void multiply_ShouldMultiplyNumbers() {

        //given
        double firstOperand1 = 10;
        double firstOperand2 = -4;
        double firstOperand3 = 12;
        double secondOperand1 = 0;
        double secondOperand2 = 33;
        double secondOperand3 = -3;

        //when
        double expected1 = firstOperand1 * secondOperand1;
        double expected2 = firstOperand2 * secondOperand2;
        double expected3 = firstOperand3 - secondOperand3;

        //then
        assertEquals(expected1, Calculator.functionMultiply(firstOperand1, secondOperand1));
        assertEquals(expected2, Calculator.functionMultiply(firstOperand2, secondOperand2));
        assertNotEquals(expected3, Calculator.functionMultiply(firstOperand3, secondOperand3));
    }

    @org.junit.jupiter.api.Test
    void divide_shouldDivideNumbers() {

        //given
        double firstOperand1 = 22;
        double firstOperand2 = -34;
        double firstOperand3 = 0;
        double secondOperand1 = -3;
        double secondOperand2 = 0;
        double secondOperand3 = 4;

        //when
        double expected1 = firstOperand1 / secondOperand1;
        double expected2 = firstOperand2 / secondOperand2;
        double expected3 = firstOperand3 - secondOperand3;

        //then
        assertEquals(expected1, Calculator.functionDivision(firstOperand1, secondOperand1));
        assertEquals(expected2, Calculator.functionDivision(firstOperand2, secondOperand2));
        assertNotEquals(expected3, Calculator.functionDivision(firstOperand3, secondOperand3));
    }
}