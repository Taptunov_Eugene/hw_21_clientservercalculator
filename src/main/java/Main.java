import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    public static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static Double firstOperand = 0d;
    public static Double secondOperand = 0d;
    public static String operator = " ";
    public static Double result = 0d;

    public static Double getResult() {
        return result;
    }

    public static void setResult(Double result) {
        Main.result = result;
    }

    public static Double getFirstOperand() {
        return firstOperand;
    }

    public static void setFirstOperand(Double firstOperand) {
        Main.firstOperand = firstOperand;
    }

    public static Double getSecondOperand() {
        return secondOperand;
    }

    public static void setSecondOperand(Double secondOperand) {
        Main.secondOperand = secondOperand;
    }

    public static String getOperator() {
        return operator;
    }

    public static void setOperator(String operator) {
        Main.operator = operator;
    }

    public static void main(String[] args) {

        int port = 8080;

        Server server = new Server(port);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        context.addServlet(new ServletHolder( new ServletCalculator( ) ),"/calculate");
        context.addServlet(new ServletHolder( new ServletHistory( ) ),"/history");
        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[] { context });
        server.setHandler(handlers);

        try {
            server.start();
            logger.info("Listening port : " + port );
            server.join();
        } catch (Exception e) {
            logger.error("Error.");
            e.printStackTrace();
        }

    }

}
