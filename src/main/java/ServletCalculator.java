
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ServletCalculator")
public class ServletCalculator extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String stra = request.getParameter("a");
        String strb = request.getParameter("b");
        String strop = request.getParameter("op");

        double a = 0;
        double b = 0;
        double result = 0;

        try {
            a = Double.parseDouble(stra);
            b = Double.parseDouble(strb);

            if (strop.equals("+")){
                result = Calculator.functionPlus(a, b);
                Main.setFirstOperand(a);
                Main.setSecondOperand(b);
                Main.setOperator("1");
                Main.setResult(result);
                doSetResult(response, result);
            }
            else if (strop.equals("-")){
                result = Calculator.functionMinus(a, b);
                Main.setFirstOperand(a);
                Main.setSecondOperand(b);
                Main.setOperator("2");
                Main.setResult(result);
                doSetResult(response, result);
            }
            else if (strop.equals("*")){
                result = Calculator.functionMultiply(a, b);
                Main.setFirstOperand(a);
                Main.setSecondOperand(b);
                Main.setOperator("3");
                Main.setResult(result);
                doSetResult(response, result);
            }
            else if (strop.equals("/") && (b != 0)) {
                result = Calculator.functionDivision(a, b);
                Main.setFirstOperand(a);
                Main.setSecondOperand(b);
                Main.setOperator("4");
                Main.setResult(result);
                doSetResult(response, result);

            } else{
                Main.setFirstOperand(0.0);
                Main.setSecondOperand(0.0);
                Main.setOperator("error");
                Main.setResult(0.0);
                doSetError(response);
            }
        } catch (Exception ex) {
            doSetError(response);
        }
    }

    protected void doSetResult(HttpServletResponse response, double result) throws IOException {
        String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes(StandardCharsets.UTF_8));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes(StandardCharsets.UTF_8));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_BAD_GATEWAY);
    }

}
