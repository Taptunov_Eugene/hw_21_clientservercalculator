public class Calculator {

    public static double functionPlus(double firstOperand, double secondOperand) {
        return firstOperand + secondOperand;
    }

    public static double functionMinus(double firstOperand, double secondOperand) {
        return firstOperand - secondOperand;
    }

    public static double functionMultiply(double firstOperand, double secondOperand) {
        return firstOperand * secondOperand;
    }

    public static double functionDivision(double firstOperand, double secondOperand) {
        return firstOperand / secondOperand;
    }
}
