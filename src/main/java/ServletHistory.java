import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/ServletHistory")
public class ServletHistory extends HttpServlet {
    public static final Logger logger = LoggerFactory.getLogger(ServletHistory.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            if (!Main.getOperator().startsWith("er") && !Main.getOperator().startsWith(" ")) {
                logger.info(String.valueOf(Main.getSecondOperand()));
                logger.info(String.valueOf(Main.getFirstOperand()));
                logger.info(Main.getOperator());
                logger.info(String.valueOf(Main.getResult()));
                doSetResult(response , Main.getSecondOperand(), Main.getFirstOperand() , Main.getOperator(), Main.getResult());
                logger.info("message: ");
            }
        } catch (Exception ex) {
            doSetError(response);
            logger.error("something went wrong");
        }
    }

    protected void doSetResult(HttpServletResponse response, double secondOperand, double firstOperand, String op, double result) throws IOException {
        String reply = "{\"error\":0,\"firstOperand\":" + Double.toString(firstOperand) + ",\"secondOperand\":" + Double.toString(secondOperand)
                + ",\"op\":" + op + ",\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write(reply.getBytes(StandardCharsets.UTF_8));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

    protected void doSetError(HttpServletResponse response) throws IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write(reply.getBytes(StandardCharsets.UTF_8));
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus(HttpServletResponse.SC_OK);
    }

}
